module.exports = function(app) {
 
	var express = require("express");
	var router = express.Router();
	
    const users = require('../controllers/user.controller.js');
	
	router.use(function (req,res,next) {
		console.log("/" + req.method);
		next();
	});
	
    // Save a User to MongoDB
    app.post('/api/user/saveuser', users.saveUser);
 
    // Retrieve all Users
    app.get('/api/user/allusers', users.findUsers);
	
	app.use("/",router);
 
}