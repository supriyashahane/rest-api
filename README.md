Commands to create rest-api and run  - 

[1] $ mkdir rest-app
[2] $ npm init
[3] $ npm install express body-parser mongoose --save
[4] $ node app.js


Directory Structure- 

/rest-api
	/app
		/config
			mongodb.config.js - mongodb connecton file
		/controllers
			user.controller.js - request to postman
		/models
			user.model.js - mongodb schema
		/routes
			user.route.js - call to GET/POST method
	/node_modules
	app.js
	package.json

Postman -

get user url - 

GET     http://127.0.0.1:8081/api/users/all

post user url - 

POST    http://127.0.0.1:8081/api/users/all

row json data - 
{"firstname":"payal","lastname":"bhosale","address":"pune"}